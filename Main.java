package cliente.servidor.tcpip;

import java.net.ServerSocket;
import java.net.Socket;

public class Main {
    public Main() {
    }

    public static void main(String[] args) {
        int PUERTO = 60001;
        System.out.print("\n Arranca Servidor");
        ServerSocket socketServidor = null;

        try {
            socketServidor = new ServerSocket(PUERTO);
        } catch (Exception var7) {
            System.out.println(var7.getMessage());
        }

        while(true) {
            Socket cliente = null;

            try {
                cliente = socketServidor.accept();
            } catch (Exception var6) {
                System.out.println(var6.getMessage());
            }

            Servidor nuevoCliente = new Servidor(cliente);
            Thread hilo = new Thread(nuevoCliente);
            hilo.start();
        }
    }
}
