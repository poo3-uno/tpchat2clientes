package cliente.servidor.tcpip;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.Socket;

public class Servidor implements Runnable {
    Socket elCliente;
    DataOutputStream salida;
    BufferedReader entrada;
    String leido;

    public Servidor(Socket cliente) {
        this.elCliente = cliente;
    }

    public void run() {
        System.out.println("\n Entro puerto=" + this.elCliente.getPort() + " ip=" + this.elCliente.getRemoteSocketAddress());

        try {
            this.salida = new DataOutputStream(this.elCliente.getOutputStream());
            this.salida.writeBytes("\nHola\n");
            Thread.sleep(5000L);

            while(true) {
                this.entrada = new BufferedReader(new InputStreamReader(this.elCliente.getInputStream()));
                System.out.print("\nEsperando");
                this.leido = this.entrada.readLine();
                System.out.print("\nLeido" + this.leido + "\n");
                this.salida.writeBytes("\nLeido" + this.leido + "\n");
            }
        } catch (Exception var2) {
            System.out.println(var2.getMessage());
        }
    }
}
